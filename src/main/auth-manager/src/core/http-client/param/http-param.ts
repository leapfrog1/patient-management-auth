interface Param {
    [key: string]: string;
}

export class HttpParam {
    private param: Param = {};
    set(key: string, value: string): HttpParam {
        this.param = { ...this.param, [key]: value };
        return this;
    }

    get(): Param {
        return this.param;
    }
}