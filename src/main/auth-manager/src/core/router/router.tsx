import React, { PureComponent } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import User from "../../pages/users/users";
import { paths } from '../../shared/constants/route.constant';
import Layout from '../layout/layout';

export interface RouteDef {
    path: string;
    actionBar?: JSX.Element,
    element: JSX.Element,
    isProtected?: boolean
}

const routes: RouteDef[] = [
    {
        path: '*',
        element: <Navigate to={paths.users} replace={true} />,
        actionBar: <User.ActionBar className="w-100"></User.ActionBar>
    },
    {
        path: paths.home,
        actionBar: <User.ActionBar className="w-100" />,
        element: <Navigate to={paths.users}  replace={true} />,
    },
    {
        path: paths.users,
        actionBar: <User.ActionBar className="w-100" />,
        element: <User />,
    }
];

class Router extends PureComponent {
    render(): React.ReactNode {
        return (
            <BrowserRouter>
                <Layout>
                    <Routes>
                        {
                            routes.map(value =>
                                <Route key={value.path} {...value} element={value.element} />
                            )
                        }
                    </Routes>
                </Layout>
            </BrowserRouter>
        );
    }
}

export default Router;
