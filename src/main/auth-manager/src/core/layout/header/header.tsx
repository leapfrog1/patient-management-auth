import { Logo } from '../../../shared/components/icons/logo/logo';

export const Header = (): JSX.Element => {
    return (
        <header className="header p-1r bg-primary flex flex--ac flex--jsb">
            <Logo/>
        </header>
    );
};
