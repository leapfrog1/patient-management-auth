import React, { ReactNode } from 'react';

export interface MainProps {
    children: ReactNode
}
const Main: React.FC<MainProps> = (props: MainProps) => {
    return (
        <main>
            {props.children}
        </main>
    );
};

export default Main;
