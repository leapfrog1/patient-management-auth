import React from 'react';
import Router from "./core/router/router";

function App(): JSX.Element {
    return (
        <Router />
    );
}

export default App;
