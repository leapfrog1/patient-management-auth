export const apiURLs = Object.freeze({
  base: 'api/v1',
  users: '/users'
});
