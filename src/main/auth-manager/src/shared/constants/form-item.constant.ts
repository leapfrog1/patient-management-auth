import {
    FormItem, pattern,
    required
} from '../components/form/validation-rules/rules';
import {
    regexTemplate,
    requiredTemplate
} from '../components/form/validation-rules/rules.constant';

export const controls = {
  email: 'username',
};

export const FormItems: Readonly<{ [key: string]: FormItem }> = {
  [controls.email]: {
    name: controls.email,
    placeholder: 'Example: john.doe@gmail.com',
    title: 'Email',
    rules: [
      {
        type: required,
        value: true,
        message: requiredTemplate('Email')
      },
      {
        type: pattern,
        value: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        message: regexTemplate('Email')
      }
    ]
  }
};
