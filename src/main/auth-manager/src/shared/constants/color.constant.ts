export const Color = Object.freeze({
  primary: '#D44C75',
  inactiveItem: '#757575',
  accent: '#C070FF'
});
