export const MessageResponse = {
  httpErrorResponse: {
    serverDown: {
      code: 0,
      message: 'Could not connect to the server',
      status: '0'
    },
    offline: {
      message: 'Offline',
      description: 'Please connect to the internet'
    }
  }
};
