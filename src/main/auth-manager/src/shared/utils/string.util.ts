export const isEmpty = (value: string | null | undefined): boolean => {
  return (value ?? '').length === 0;
};
