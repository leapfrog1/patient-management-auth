export const hasValue = <T>(value: T): boolean => {
  return value !== null && value !== undefined;
};
