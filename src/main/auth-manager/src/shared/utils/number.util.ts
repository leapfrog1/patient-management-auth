export const isNumeric = (value: string): boolean => {
  return !isNaN(+value) && !isNaN(parseFloat(value));
};
