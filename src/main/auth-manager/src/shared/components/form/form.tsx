import React, { FormEvent, FormHTMLAttributes } from 'react';

export interface FormProps {
  className?: string;
  children?: React.ReactNode;
  baseProps?: FormHTMLAttributes<HTMLFormElement>;
  submit?: (value: FormData) => void;
}

export interface FormData {
  form: HTMLFormElement,
  data: object;
}

const toFormData = (formEvent: FormEvent<HTMLFormElement>): FormData => {
  formEvent.preventDefault();
  return {
    form: formEvent.currentTarget,
    data: Object.fromEntries(new FormData(formEvent.currentTarget).entries())
  };
};

export const Form: React.FC<FormProps> = ({ submit, children, baseProps, className }: FormProps) => {
  return (
    <form onSubmit={($event) => submit?.(toFormData($event))} className={className} {...baseProps}>
      {children}
    </form>
  );
};
