export const requiredTemplate = (name: string): string => `${name} cannot be empty`;

export const minLengthTemplate = (name: string, value: string): string =>
  `${name} cannot be less than ${value} characters`;

export const maxLengthTemplate = (name: string, value: string): string =>
  `${name} cannot be greater than ${value} characters`;

export const maxValueTemplate = (name: string, value: string): string =>
  `${name} cannot be greater than ${value} characters`;

export const minValueTemplate = (name: string, value: string): string =>
  `${name} cannot be less than ${value} characters`;

export const regexTemplate = (name: string): string => `Please enter valid ${name}`;
