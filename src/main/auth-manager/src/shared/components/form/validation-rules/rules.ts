import { isNumeric } from '../../../utils/number.util';
import { isEmpty } from '../../../utils/string.util';

export const minLength = 'MIN_LENGTH';
export const maxLength = 'MAX_LENGTH';
export const min = 'MIN';
export const max = 'MAX';
export const pattern = 'PATTERN';
export const required = 'REQUIRED';
export type validationTypes = 'MIN_LENGTH' | 'MAX_LENGTH' | 'MIN' | 'MAX' | 'PATTERN' | 'REQUIRED';

export interface ValidationRule {
  type: validationTypes;
  value: string | number | boolean | RegExp;
  message: string;
}

export interface FormItem {
  name: string;
  placeholder?: string;
  title: string;
  rules: ValidationRule[];
}

export const validator = (formItem: FormItem) => {
  return (value: string) => {
    const requiredValidation = formItem.rules.find((v) => v.type === 'REQUIRED');
    if (requiredValidation?.value) {
      const message = validate(requiredValidation.type)(requiredValidation, value);
      if (message) {
        return message;
      }
    }
    if (!isEmpty(value)) {
      for (const rule of formItem.rules) {
        const errorMessage = validate(rule.type)(rule, value);
        if (errorMessage) {
          return errorMessage;
        }
      }
    }
  };
};

const validate = (validationType: validationTypes): any => {
  return {
    [minLength]: validateMinLength,
    [maxLength]: validateMaxLength,
    [min]: validateMin,
    [max]: validateMax,
    [pattern]: validatePattern,
    [required]: validateRequired
  }[validationType];
};

const validateRequired = (
  rule: ValidationRule,
  value: string | number | boolean
): string | null => {
  if (isEmpty(value?.toString())) {
    return rule.message;
  }
  return null;
};

const validateMinLength = (rule: ValidationRule, value: string | number | boolean): string | null => {
  assertNotEmpty(rule.value.toString(), rule);
  assertNumber(rule.value.toString(), rule);
  if (value.toString().length < +rule.value) {
    return rule.message;
  }
  return null;
};

const validateMaxLength = (rule: ValidationRule, value: string | number | boolean): string | null => {
  assertNotEmpty(rule.value.toString(), rule);
  assertNumber(rule.value.toString(), rule);
  if (value.toString().length > +rule.value) {
    return rule.message;
  }
  return null;
};

const validateMin = (rule: ValidationRule, value: string | number | boolean): string | null => {
  assertNotEmpty(rule.value.toString(), rule);
  assertNumber(rule.value.toString(), rule);
  assertNumber(value, rule);
  if (+value < +rule.value) {
    return rule.message;
  }
  return null;
};

const validateMax = (rule: ValidationRule, value: string | number | boolean): string | null => {
  assertNotEmpty(rule.value.toString(), rule);
  assertNumber(rule.value.toString(), rule);
  assertNumber(value, rule);
  if (+value > +rule.value) {
    return rule.message;
  }
  return null;
};

const validatePattern = (rule: ValidationRule, value: string | number | boolean): string | null => {
  assertNotEmpty(rule.value.toString(), rule);
  const regex: RegExp = new RegExp(rule.value as RegExp);
  if (!regex.test(value.toString())) {
    return rule.message;
  }
  return null;
};

const assertNumber = (value: string | number | boolean, rule: ValidationRule): void => {
  if (!isNumeric(value.toString())) {
    throw new Error(`Value of ${rule.type} must be a number`);
  }
};

const assertNotEmpty = (value: string | number | boolean, rule: ValidationRule): void => {
  if (isEmpty(value.toString())) {
    throw new Error(`Value of ${rule.type} must not be empty`);
  }
};
