import { BaseInputProps } from '../base-form-item/base-input';

const Label = ({ formItem, labelProps }: BaseInputProps): JSX.Element | null => {
    if (formItem?.title) {
        return (
            <label className='font-w6' {...labelProps} htmlFor={labelProps?.htmlFor}>{formItem?.title}</label>
        );
    }
    return null;
};

export default Label;
