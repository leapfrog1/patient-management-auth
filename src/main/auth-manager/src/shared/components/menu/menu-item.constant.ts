import { paths } from '../../constants/route.constant';
import { IconProps } from '../icons/svg-icon';
import Icons from '../icons/svg-icon.constant';

export interface MenuProps {
  name: string
  icon: IconProps
  link: string
  active: boolean
  menuChange?: (menu: MenuProps) => void
}

const Menus: MenuProps[] = [
  Object.freeze({
    name: 'Users',
    link: paths.users,
    icon: Icons.users,
    active: false
  })
];

export default structuredClone(Menus) as MenuProps[];
