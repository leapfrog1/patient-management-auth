import * as React from 'react';

interface MenuProps {
    children: React.ReactNode
}
export const Menu: React.FunctionComponent<MenuProps> = props => {
    return (
        <div className="menu w-100 bg-primary primary-border position-abs  p-1r">
            {props.children}
        </div>
    );
};
