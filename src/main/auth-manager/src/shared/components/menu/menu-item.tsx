import React from 'react';
import { Link } from 'react-router-dom';
import { Color } from '../../constants/color.constant';
import Icon from '../icons/svg-icon';
import { MenuProps } from './menu-item.constant';

const MenuItem: React.FunctionComponent<MenuProps> = ({ name, link, icon, active, menuChange }: MenuProps) => {
    return (
        <Link to={link} onClick={() => menuChange?.({ name, link, icon, active })} className={`menu-item txt-decoration-none p-1r flex flex-alc ${active ? 'menu-item--active' : 'menu-item--inactive'}`}>
            <Icon {...icon} stroke={active ? Color.accent : icon.stroke}></Icon>
            <span className={'menu-item__title ml-1r'}>{name}</span>
        </Link>
    );
};

export default MenuItem;
