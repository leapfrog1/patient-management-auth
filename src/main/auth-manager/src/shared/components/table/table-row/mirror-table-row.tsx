import React from 'react';
import { TRProps } from './table-row';

export interface MtrProps extends TRProps {
}

const MTR: React.FC<TRProps> = (props: MtrProps): JSX.Element => {
  return (
    <tr className='interactive-row'>
      {props.children}
    </tr>
  );
};

export default MTR;
