import { HTMLAttributes } from 'react';
import Icon from '../../icons/svg-icon';
import Icons from '../../icons/svg-icon.constant';

export interface NextPageProps {
    goToNextPage: () => void
}

const GoToNextPage: React.FC<NextPageProps & HTMLAttributes<HTMLButtonElement>> = ({ goToNextPage, ...props }: NextPageProps & HTMLAttributes<HTMLButtonElement>) => {
    return (
        <button onClick={() => goToNextPage()} {...props} className={`basic-round-button ${props?.className ?? ''}`}>
            <Icon {...Icons.nextIndicator}></Icon>
        </button>
    );
};

export default GoToNextPage;
