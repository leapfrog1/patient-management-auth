import { HTMLAttributes } from 'react';
import Icon from '../../icons/svg-icon';
import Icons from '../../icons/svg-icon.constant';

export interface LastPageProps {
  goToLastPage: () => void
}

const GoToLastPage: React.FC<LastPageProps & HTMLAttributes<HTMLButtonElement>> = ({ goToLastPage, ...props }: LastPageProps & HTMLAttributes<HTMLButtonElement>) => {
  return (
        <button onClick={() => goToLastPage()} {...props} className={`basic-round-button ${props?.className ?? ''}`}>
            <Icon {...Icons.lastIndicator}></Icon>
        </button>
  );
};

export default GoToLastPage;
