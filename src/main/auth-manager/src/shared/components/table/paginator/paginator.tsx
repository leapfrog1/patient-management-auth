import React from 'react';
import GoToFirstPage from './first-page';
import GoToLastPage from './last-page';
import GoToNextPage from './next-page';
import PageIndicators, { PageNumberProps } from './page-numbers';
import GoToPrevPage from './prev-page';

const Paginator: React.FC<PageNumberProps> = (props: PageNumberProps) => {
  const { pageable, pageChange } = props;
  const { activePage, totalPages } = pageable;
  return (
    <div className="paginator flex flex--jsc">
      <GoToFirstPage goToFirstPage={() => props.pageChange?.(1)} className="mr-1r" />
      <GoToPrevPage goToPrevPage={() => pageChange?.(activePage === 1 ? activePage : activePage - 1)} className="mr-1r" />
      <PageIndicators {...props} pageChange={(pageNumber) => pageChange?.(pageNumber)}></PageIndicators>
      <GoToNextPage goToNextPage={() => pageChange?.(activePage === totalPages ? activePage : activePage + 1)} className="mr-1r" />
      <GoToLastPage goToLastPage={() => pageChange?.(totalPages)} />
    </div>
  );
};

export default Paginator;
