import { HTMLAttributes } from 'react';
import Icon from '../../icons/svg-icon';
import Icons from '../../icons/svg-icon.constant';

export interface PrevPageProps {
    goToPrevPage: () => void
}

const GoToPrevPage: React.FC<PrevPageProps & HTMLAttributes<HTMLButtonElement>> = ({ goToPrevPage, ...props }: PrevPageProps & HTMLAttributes<HTMLButtonElement>) => {
    return (
        <button onClick={() => goToPrevPage()} {...props} className={`basic-round-button ${props?.className ?? ''}`}>
            <Icon {...Icons.previousIndicator}></Icon>
        </button>
    );
};

export default GoToPrevPage;
