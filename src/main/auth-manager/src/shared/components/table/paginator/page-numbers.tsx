const midNumbersSize = 3;

export interface Pageable {
  totalPages: number
  pageCapacity: number
  activePage: number
}

export interface PageNumberProps {
  pageable: Pageable
  pageChange?: (pageNumber: number) => void
}

const fixedInitialNumbers: readonly number[] = [1, 2];

const fixedFinalNumbers = (totalPages: number): readonly number[] => {
  return [totalPages - 1, totalPages];
};

const calcRollingNumbers = (activePage: number, totalPages: number): number[] => {
  let numberStarter = fixedFinalNumbers(totalPages).at(-1)! - midNumbersSize;
  if (activePage >= numberStarter) {
    return [numberStarter - 2, numberStarter - 1, numberStarter, numberStarter + 1];
  }
  numberStarter = fixedInitialNumbers.at(0)! + midNumbersSize;
  if (activePage <= numberStarter) {
    return [numberStarter - 1, numberStarter, numberStarter + 1, numberStarter + 2];
  }
  return [activePage - 2, activePage - 1, activePage, activePage + 1, activePage + 2];
};

const rollingNumbersWithEllipsis = (activePage: number, totalPages: number): Array<string | number> => {
  let rollingNumbers: Array<number | string> = calcRollingNumbers(activePage, totalPages);
  if (+rollingNumbers.at(0)! - 1 !== fixedInitialNumbers.at(-1)!) {
    rollingNumbers = ['...', ...rollingNumbers];
  }
  if (+rollingNumbers.at(-1)! + 1 !== fixedFinalNumbers(totalPages).at(0)!) {
    rollingNumbers.push('...');
  }
  return rollingNumbers;
};

const visibleNumbers = (totalPages: number, activePage: number): Array<string | number> => {
  if (totalPages <= 7) {
    return new Array(totalPages).fill(0).map((_, index) => index + 1);
  }
  return [...fixedInitialNumbers, ...rollingNumbersWithEllipsis(activePage, totalPages), ...fixedFinalNumbers(totalPages)];
};

const renderPageIndicators = (activePage: number, pageIndicators: Array<number | string>, { pageChange }: PageNumberProps): JSX.Element => {
  return (
    <ul className="horizontal-list mr-2r">
      {
        pageIndicators.map((value, index) =>
          <li key={value.toString() + index.toString()} onClick={() => typeof value === 'number' ? pageChange?.(+value) : null} className={`page-indicator ${value === activePage ? 'page-indicator--active' : ''}`}>
            <a href='#' className="page-indicator__text txt-decoration-none">{value}</a>
          </li>
        )
      }
    </ul>
  );
};

const PageIndicators: React.FC<PageNumberProps> = (props: PageNumberProps) => {
  const { pageable } = props;
  const { activePage, totalPages } = pageable;
  return (
    renderPageIndicators(activePage, visibleNumbers(totalPages, activePage), props)
  );
};

export default PageIndicators;
