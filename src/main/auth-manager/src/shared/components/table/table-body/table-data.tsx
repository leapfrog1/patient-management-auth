import React from 'react';

export interface BaseTableData {
  datum: JSX.Element | string | number | JSX.Element[]
}

const TD: React.FunctionComponent<BaseTableData> = ({ datum }: BaseTableData) => {
  return (
    <td className="base-td">{datum}</td>
  );
};

export default TD;
