import { Pageable } from '../paginator/page-numbers';

export interface PageableData<T> {
  count: number;
  totalPages?: number;
  data: T[];
}

export function emptyData<T>(): PageableData<T> {
  return Object.freeze({
    count: 0,
    totalPages: 0,
    data: []
  });
}

const INITIAL_PAGE_DATA: Readonly<Pageable> = {
  activePage: 1,
  pageCapacity: 10,
  totalPages: 0
};

export const initialPage = (): Pageable => ({ ...INITIAL_PAGE_DATA });
