import React from 'react';
import TH, { BaseColumn } from './table-header/table-header';
import TR from './table-row/table-row';

export interface ColumnData {
  [key: string]: JSX.Element | string | JSX.Element[] | number;
}
export interface BasicTableProps {
  columns: BaseColumn[];
  data: JSX.Element[];
}

export interface BaseTable {
  renderColumn: () => JSX.Element
  renderTD: () => JSX.Element[]
}

export class BasicTable extends React.PureComponent<BasicTableProps> implements BaseTable {
  get columns(): BaseColumn[] {
    return this.props.columns;
  }

  renderColumn(): JSX.Element {
    return (
      <TR>
        {
          this.columns.map(column =>
            <TH {...column} key={column.name}></TH>
          )
        }
      </TR>

    );
  }

  renderTD(): JSX.Element[] {
    return this.props.data;
  }

  render(): React.ReactNode {
    return (
      <table className="basic-table">
        <thead>
          {
            this.renderColumn()
          }
        </thead>
        <tbody>
          {
            this.renderTD()
          }
        </tbody>
      </table>
    );
  }
}
