import React from 'react';

export interface BaseColumn {
    name: string
    title: string
}

const TH: React.FunctionComponent<BaseColumn> = ({ title, name }: BaseColumn) => {
    return (
        <th className="base-th" key={name}>{title}</th>
    );
};

export default TH;
