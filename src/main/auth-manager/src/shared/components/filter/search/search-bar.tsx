import * as React from 'react';
import { Form, FormData } from '../../form/form';

export interface SearchBarProps {
  className?: string;
  inputProps?: React.InputHTMLAttributes<HTMLInputElement>;
  buttonProps?: React.ButtonHTMLAttributes<HTMLButtonElement>;
  search?: (query: string) => void
}
const SearchBar: React.FC<SearchBarProps> = ({ search, className, inputProps, buttonProps }) => {
  return (
    <Form submit={(value: FormData) => search?.((value.data as { query: string }).query)} className={`flex ${className ?? ''}`}>
      <input {...inputProps} name='query' className='basic-input w-40' type='search' />
      <button {...buttonProps} className='primary-button search-button'>Search</button>
    </Form>
  );
};

export default SearchBar;
