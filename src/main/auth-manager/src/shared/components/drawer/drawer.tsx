import React, { HTMLAttributes } from 'react';

export interface DrawerProps extends HTMLAttributes<HTMLElement> {
    open: boolean
    closeDrawer?: () => void
    children?: React.ReactNode
}

const Drawer: React.FC<DrawerProps> = ({ open, closeDrawer, children, className, ...props }: DrawerProps) => {
    if (open) {
        return (
            <div onClick={() => closeDrawer?.()} {...props} className={`drawer-container z-1000 ${className ?? ''}`}>
                <div onClick={(event) => event.stopPropagation()} className="drawer">
                    {children}
                </div>
            </div>
        );
    }
    return null;
};

export default Drawer;
