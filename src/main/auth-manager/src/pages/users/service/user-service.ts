import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { PageableData } from '../../../shared/components/table/model/pageable';
import { Pageable } from '../../../shared/components/table/paginator/page-numbers';
import { apiURLs } from '../../../shared/constants/api-url.constant';
import User from '../model/user';

const { base, users } = apiURLs;
const userApi = base + users;
export const userService = createApi({
  reducerPath: 'userService',
  baseQuery: fetchBaseQuery(),
  refetchOnMountOrArgChange: 0.1,
  tagTypes: ['USER'],
  endpoints: (builder) => ({
    getPageableData: builder.query<
      PageableData<User>,
      Pageable & { query?: string }
    >({
      query: ({ activePage, pageCapacity, query }) =>
        `${userApi}?page=${activePage - 1}&size=${pageCapacity}&query=${
          query ?? ''
        }&sort=id,desc`,
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({ type: 'USER' as const, id })),
              { type: 'USER', id: 'LIST' },
            ]
          : [{ type: 'USER', id: 'LIST' }],
    }),
    saveUser: builder.mutation<User, User>({
      query: (body) => ({
        url: `${userApi}`,
        method: 'POST',
        body,
      }),
      invalidatesTags: [{ type: 'USER', id: 'LIST' }],
    }),
  }),
});

export const { useGetPageableDataQuery, useSaveUserMutation } = userService;
