import React from 'react';
import UserList from "./table/user-list";
import UserActionWidget from "./action-widget/users-action-widget";

class User extends React.Component {
    render(): React.ReactNode {
        return (
            <UserList/>
        );
    }
}

export default Object.assign(User, {
    ActionBar: UserActionWidget
});