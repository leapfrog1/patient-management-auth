import React, { HTMLAttributes } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../../core/root-redux/store';
import Drawer from '../../../shared/components/drawer/drawer';
import SearchBar from '../../../shared/components/filter/search/search-bar';
import UserForm from '../form/user-form';
import { changePage, search, toggleDrawer } from '../redux/user-slice';

const UserActionWidget: React.FC<HTMLAttributes<{}>> = (props) => {
    const { drawerState, paginator } = useSelector((state: RootState) => state.user);
    const dispatch = useAppDispatch();
    const onSearch = (newQuery: string): void => {
        dispatch(search(newQuery));
        dispatch(changePage({ pageable: { ...paginator.pageable, activePage: 1 } }));
    };
    return (
        <div className={`flex ${props.className ?? ''}`}>
            <Drawer closeDrawer={() => dispatch(toggleDrawer(null))} open={drawerState}>
                <UserForm></UserForm>
            </Drawer>
            <SearchBar search={onSearch} inputProps={{
                placeholder: 'Search by Email...'
            }} className="w-100" />
            <button onClick={() => dispatch(toggleDrawer(null))} className="primary-button primary-button--outline">Add User</button>
        </div>
    );
};

export default UserActionWidget;
