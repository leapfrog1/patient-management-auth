import React, { FC, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../../core/root-redux/store';
import Email from '../../../shared/components/form/email/email';
import { Form, FormData } from '../../../shared/components/form/form';
import { controls, FormItems } from '../../../shared/constants/form-item.constant';
import User, { defaultUserData } from '../model/user';
import { changePage, search } from '../redux/user-slice';
import { useSaveUserMutation } from '../service/user-service';

const setFormProps = (formData: User, formKey: string, setFormData: (value: React.SetStateAction<User>) => void): React.InputHTMLAttributes<HTMLInputElement> => {
    return {
        value: (formData?.[formKey as keyof User]?.toString() ?? ''), onInput: (event) => setFormData({ ...formData, [formKey]: event.currentTarget.value })
    };
};

const UserForm: FC = () => {
    const dispatch = useAppDispatch();
    const { paginator, formData } = useSelector((state: RootState) => state.user);
    const [newFormData, setFormData] = useState(formData);
    const [saveUser] = useSaveUserMutation(defaultUserData as Object);
    const isUpdate = (newFormData?.id ?? 0) > 0;
    const onSubmit = ({ form, data }: FormData): void => {
        if (!isUpdate) {
            form.reset();
            saveUser(data as User).then(() => {
                setFormData(defaultUserData);
                dispatch(changePage({ pageable: { ...paginator.pageable, activePage: 1 } }));
                dispatch(search(''));
            });
        } else {
            saveUser({ ...data as User, id: formData.id });
        }
    };
    const { email } = controls;
    return (
        <Form baseProps={{ className: 'pt-2r pb-2r' }} submit={(event: FormData) => onSubmit(event)}>
            <Email inputProps={setFormProps(newFormData, email, setFormData)} formItem={FormItems[email]}></Email>
            <div className="flex flex--jsc w-100 mt-2r">
                <button type='submit' className="primary-button pl-3r pr-3r p-1r mr-1r">{isUpdate ? 'Update User' : 'Add User'}</button>
            </div>
        </Form >
    );
};

export default UserForm;
