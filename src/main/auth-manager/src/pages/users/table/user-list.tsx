import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../core/root-redux/store';
import { initialPage, PageableData } from '../../../shared/components/table/model/pageable';
import { PageNumberProps } from '../../../shared/components/table/paginator/page-numbers';
import Paginator from '../../../shared/components/table/paginator/paginator';
import { BasicTable } from '../../../shared/components/table/table';
import User from '../model/user';
import { changePage, openDrawerWith } from '../redux/user-slice';
import { useGetPageableDataQuery } from '../service/user-service';
import { toBasicTableProps } from './user-mapper';

const renderTable = (page: PageNumberProps, data: PageableData<User>, dispatch: (action: any) => void): JSX.Element => {
  return (
    <div>
      <BasicTable {...toBasicTableProps(data, (data) => dispatch(openDrawerWith(data)))}/>
      <div className="mt-2r">
        <Paginator {...page} pageChange={activePage => dispatch(changePage({pageable: {...page.pageable, activePage}}))} />
      </div>
    </div>
  );
};

const renderLoader = (): JSX.Element => {
  return (
    <div>Loading....</div>
  );
};

const renderDataError = (): JSX.Element => {
  return (
    <div>Error while fetching data</div>
  );
};

const renderBlankData = (): JSX.Element => {
  return (
    <div>
      Data not found
    </div>
  );
};

const UserList = (): JSX.Element => {
  const dispatch = useDispatch();
  const { query, paginator } = useSelector((state: RootState) => state.user);
  const { data, error, isLoading } = useGetPageableDataQuery({ ...initialPage(), activePage: paginator.pageable.activePage, query });
  if (!data || error) {
    return renderDataError();
  }
  if (isLoading) {
    return renderLoader();
  }
  if (data && data.data?.length === 0) {
    return renderBlankData();
  }
  return renderTable(paginator, data, dispatch);
};

export default UserList;
