import {PageableData} from '../../../shared/components/table/model/pageable';
import {BasicTableProps} from '../../../shared/components/table/table';
import TD from '../../../shared/components/table/table-body/table-data';
import TR from '../../../shared/components/table/table-row/table-row';
import userColumns from '../constants/user.constant';
import User from '../model/user';

export const toBasicTableProps = (users: PageableData<User>, onRowClick: (user: User) => void): BasicTableProps => {
  return {
    columns: userColumns,
    data: users.data.map(value => {
      return <TR key={value.id}>
        <TD datum={value.username}/>
      </TR>;
    })
  };
};
