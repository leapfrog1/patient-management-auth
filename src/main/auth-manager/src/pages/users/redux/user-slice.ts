import { createSlice, PayloadAction, SliceCaseReducers } from '@reduxjs/toolkit';
import {
  emptyData,
  initialPage,
  PageableData
} from '../../../shared/components/table/model/pageable';
import { PageNumberProps } from '../../../shared/components/table/paginator/page-numbers';
import User, { defaultUserData } from '../model/user';
import { userService } from '../service/user-service';

const paginator: PageNumberProps = {
  pageable: initialPage()
};

interface UserState {
  paginator: PageNumberProps;
  query: string;
  drawerState: boolean;
  data: PageableData<User>;
  formData: User;
}

export const userSlice = createSlice<UserState, SliceCaseReducers<UserState>, string>({
  name: 'User',
  initialState: {
    paginator,
    query: '',
    drawerState: false,
    data: emptyData<User>(),
    formData: defaultUserData
  },
  reducers: {
    toggleDrawer: (state) => {
      state.drawerState = !state.drawerState;
      state.formData = defaultUserData;
    },
    search: (state, action: PayloadAction<string>) => {
      state.query = action.payload;
    },
    changePage: (state, action: PayloadAction<PageNumberProps>) => {
      state.paginator = action.payload;
    },
    openDrawerWith: (state, action: PayloadAction<User>) => {
      state.formData = action.payload;
      state.drawerState = true;
    }
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      userService.endpoints.getPageableData.matchFulfilled,
      (state, { payload }) => {
        state.paginator.pageable.totalPages = payload.totalPages ?? 1;
        state.data = payload;
      }
    );
  }
});

export const { toggleDrawer, search, changePage, openDrawerWith } = userSlice.actions;

export default userSlice.reducer;
