import { BaseColumn } from '../../../shared/components/table/table-header/table-header';

const userColumns: BaseColumn[] = [
  {
    name: 'username',
    title: 'Email'
  }
];

export default structuredClone(userColumns);
