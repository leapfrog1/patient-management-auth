import {Identity} from "../../../shared/models/identity";

interface User extends Identity<number>{
    username: string;
}

export const defaultUserData: User = Object.freeze({
    id: 0,
    username: '',
});


export default User;