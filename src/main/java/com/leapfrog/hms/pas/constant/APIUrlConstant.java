package com.leapfrog.hms.pas.constant;

public final class APIUrlConstant {
    private APIUrlConstant() {
    }

    public static final String BASE_API_V1 = "api/v1";
    public static final String USERS = "/users";
    public static final String USERS_FULL = BASE_API_V1 + USERS;

}
