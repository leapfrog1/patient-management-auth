package com.leapfrog.hms.pas.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("pms.client")
@Data
public class PMSClientProperties {
    private String uri;
    private String loginSuccessUri;
    private String logoutSuccessUri;
    private String issuerUri;

}
