package com.leapfrog.hms.pas.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("pms.email")
@Data
public class EmailProperties {
    private Credentials credentials;
    private Region region;

    @Data
    public static class Credentials {
        private String username;
        private String password;
    }

    @Data
    public static class Region {
        private String name;
    }
}