package com.leapfrog.hms.pas.mapper;

import com.leapfrog.hms.pas.dto.UserDTO;
import com.leapfrog.hms.pas.entity.UserEntity;
import com.leapfrog.hms.pas.mapper.base.BaseMapper;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserDTO, UserEntity> {
}
