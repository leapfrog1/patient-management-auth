package com.leapfrog.hms.pas.configuration;

import com.leapfrog.hms.pas.props.PMSClientProperties;
import com.leapfrog.hms.pas.service.CustomAuthenticationProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutHandler;

@EnableWebSecurity
@RequiredArgsConstructor
public class DefaultConfiguration {

    private final CustomAuthenticationProvider customAuthenticationProvider;
    private final LogoutHandler logoutHandler;
    private final PMSClientProperties clientProperties;

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        return http
                .csrf()
                .disable()
                .authorizeRequests(
                        authorizeRequests -> authorizeRequests
                                .antMatchers("/oauth2/**")
                                .permitAll()
                                .anyRequest()
                                .fullyAuthenticated()
                )
                .formLogin(login -> login.defaultSuccessUrl(clientProperties.getLoginSuccessUri()).permitAll())
                .logout()
                .addLogoutHandler(logoutHandler)
                .logoutSuccessUrl(clientProperties.getLogoutSuccessUri())
                .permitAll()
                .and()

                .build();
    }

    @Autowired
    public void bindAuthenticationProvider(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder
                .authenticationProvider(customAuthenticationProvider);
    }
}
