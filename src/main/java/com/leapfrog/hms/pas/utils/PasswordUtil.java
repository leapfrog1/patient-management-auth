package com.leapfrog.hms.pas.utils;

import java.security.SecureRandom;

public class PasswordUtil {

    private PasswordUtil() {}

    public static char[] generatePassword(int size)
    {
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < size; i++)
        {
            int randomIndex = random.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex));
        }

        return sb.toString().toCharArray();
    }
}
