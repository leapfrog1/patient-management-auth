package com.leapfrog.hms.pas.utils;

import com.leapfrog.hms.pas.dto.PagedData;
import com.leapfrog.hms.pas.mapper.base.BaseMapper;
import com.leapfrog.hms.pas.repository.CustomPageableRepository;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class PageableDataDelicate<D, E, ID, PD extends PagedData<D>> {

    private CustomPageableRepository<E, ID> repository;
    private BaseMapper<D, E> baseMapper;

    public PD getData(Supplier<Page<ID>> ids, BiFunction<Page<ID>, List<D>, PD> createPagedData) {
        Page<ID> pageIds = ids.get();
        List<D> data = repository.findAllByIdIn(pageIds.getSort(), pageIds.getContent())
                .stream()
                .map(baseMapper::toDTO)
                .collect(Collectors.toList());
        return createPagedData.apply(pageIds, data);
    }

    public PageableDataDelicate<D, E, ID, PD> setMapper(final BaseMapper<D, E> baseMapper) {
        this.baseMapper = baseMapper;
        return this;
    }

    public PageableDataDelicate<D, E, ID, PD> setRepository(final CustomPageableRepository<E, ID> repository) {
        this.repository = repository;
        return this;
    }
}
