package com.leapfrog.hms.pas.controller;

import com.leapfrog.hms.pas.constant.APIUrlConstant;
import com.leapfrog.hms.pas.dto.PagedData;
import com.leapfrog.hms.pas.dto.UserDTO;
import com.leapfrog.hms.pas.service.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(APIUrlConstant.USERS_FULL)
@RequiredArgsConstructor
public class UserController {
    private final CustomUserDetailsService service;

    @GetMapping
    public ResponseEntity<PagedData<UserDTO>> getAllBy(@RequestParam(required = false) String query, Pageable pageable) {
        return ResponseEntity.ok(service.getPageableData(pageable, query));
    }

    @PostMapping
    public ResponseEntity<UserDTO> save(@RequestBody UserDTO user) {
        return ResponseEntity.ok(service.save(user));
    }

}
