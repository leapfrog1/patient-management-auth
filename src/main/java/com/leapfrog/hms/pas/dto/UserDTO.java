package com.leapfrog.hms.pas.dto;

import com.leapfrog.hms.pas.dto.base.Identity;
import lombok.Data;

@Data
public class UserDTO extends Identity<Long> {
    private String username;
}
