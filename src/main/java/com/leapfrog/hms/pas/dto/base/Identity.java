package com.leapfrog.hms.pas.dto.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class Identity<T extends Serializable> {
    private T id;
}
