package com.leapfrog.hms.pas.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserEmail {
    private String subject;
    private String address;
    private String message;
}
