package com.leapfrog.hms.pas.service.impl;

import com.leapfrog.hms.pas.dto.PagedData;
import com.leapfrog.hms.pas.dto.UserDTO;
import com.leapfrog.hms.pas.dto.UserEmail;
import com.leapfrog.hms.pas.entity.UserEntity;
import com.leapfrog.hms.pas.mapper.UserMapper;
import com.leapfrog.hms.pas.props.EmailProperties;
import com.leapfrog.hms.pas.repository.UserRepository;
import com.leapfrog.hms.pas.service.CustomUserDetailsService;
import com.leapfrog.hms.pas.service.EmailSender;
import com.leapfrog.hms.pas.utils.PageableData;
import com.leapfrog.hms.pas.utils.PasswordUtil;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

    private final UserRepository repository;
    private final UserMapper mapper = Mappers.getMapper(UserMapper.class);
    private final EmailSender emailSender;
    private final PasswordEncoder passwordEncoder;
    private final EmailProperties emailProperties;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = repository.findByUsername(username).orElseThrow(() -> {
            throw new UsernameNotFoundException("User not found");
        });

        return User.builder()
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .authorities("test")
                .build();
    }

    @Override
    public PagedData<UserDTO> getPageableData(Pageable pageable, String query) {
        return new PageableData<UserDTO, UserEntity, Long>()
                .setMapper(mapper)
                .setRepository(repository)
                .get(() -> repository.findAllIdsBy(pageable, (query == null ? "" : query).replaceAll("\\s", "")));
    }

    @Override
    public UserDTO save(UserDTO user) {
        UserEntity persistableUser = mapper.toEntity(user);
        char[] password = PasswordUtil.generatePassword(15);
        persistableUser.setPassword(passwordEncoder.encode(new String(password)));
        persistableUser = repository.save(persistableUser);

        UserEmail emailUser = UserEmail
                .builder()
                .address(user.getUsername())
                .message("Your password for PMS is: " + new String(password))
                .subject("Account created")
                .build();
        emailSender.sendEmail(emailProperties.getCredentials().getUsername(), emailUser);

        return mapper.toDTO(persistableUser);
    }
}
