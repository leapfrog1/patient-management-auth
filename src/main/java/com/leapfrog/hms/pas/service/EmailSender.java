package com.leapfrog.hms.pas.service;

import com.leapfrog.hms.pas.dto.UserEmail;

public interface EmailSender {
    void sendEmail(String from, UserEmail user);
}
