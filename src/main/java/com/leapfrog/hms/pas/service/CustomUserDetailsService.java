package com.leapfrog.hms.pas.service;

import com.leapfrog.hms.pas.dto.PagedData;
import com.leapfrog.hms.pas.dto.UserDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface CustomUserDetailsService extends UserDetailsService {
    PagedData<UserDTO> getPageableData(Pageable pageable, String query);
    UserDTO save(UserDTO user);
}
