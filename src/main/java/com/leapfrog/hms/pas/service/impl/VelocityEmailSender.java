package com.leapfrog.hms.pas.service.impl;

import com.leapfrog.hms.pas.dto.UserEmail;
import com.leapfrog.hms.pas.service.EmailSender;
import lombok.RequiredArgsConstructor;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.io.StringWriter;

@Service
@RequiredArgsConstructor
public class VelocityEmailSender implements EmailSender {

    private final JavaMailSender mailSender;
    private final VelocityEngine velocityEngine;

    @Override
    public void sendEmail(String from, UserEmail user) throws MailException {
        mailSender.send((mimeMessage) -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(new InternetAddress(from));
            messageHelper.setTo(user.getAddress());
            messageHelper.setSubject(user.getSubject());

            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("user", user);

            StringWriter stringWriter = new StringWriter();

            velocityEngine.mergeTemplate("velocity/password-template.vm", "UTF-8", velocityContext, stringWriter);
            messageHelper.setText(stringWriter.toString(), true);
        });
    }
}
