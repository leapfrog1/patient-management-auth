package com.leapfrog.hms.pas.service;

import org.springframework.security.authentication.AuthenticationProvider;

public interface CustomAuthenticationProvider extends AuthenticationProvider {
}
