package com.leapfrog.hms.pas.repository;

import com.leapfrog.hms.pas.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends CustomPageableRepository<UserEntity, Long> {
   Optional<UserEntity> findByUsername(String username);

   @Query(Queries.USER_LIST)
   Page<Long> findAllIdsBy(Pageable pageable, String query);


   final class Queries {

      public static final String USER_LIST = "select u.id from UserEntity u where u.enabled = true" +
              " and " +
              "u.username like %:query% ";

      private Queries() {
      }
   }
}
