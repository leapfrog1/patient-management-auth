package com.leapfrog.hms.pas.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface CustomPageableRepository<T, ID> extends JpaRepository<T, ID> {


    List<T> findAllByIdIn(Sort sort, Iterable<ID> ids);

}
