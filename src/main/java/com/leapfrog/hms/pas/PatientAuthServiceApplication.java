package com.leapfrog.hms.pas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class PatientAuthServiceApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(PatientAuthServiceApplication.class, args);
    }

}
